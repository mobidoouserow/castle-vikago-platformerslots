//
//  ViewController.m
//  PlatformerSlots
//
//  Created by Pavel Wasilenko on 01/08/2017.
//  Copyright © 2017 Vik. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIWebView *mainView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSString *path = [NSBundle.mainBundle pathForResource:@"index"
                                                   ofType:@"html"
                                             inDirectory:@"Game"];
    
    NSURL *urlURL = [NSURL fileURLWithPath:path];
    
    NSURLRequest * req = [NSURLRequest requestWithURL:urlURL];
    
    //    //Cloack
    //    CastleSlotApiClient *api = [CastleSlotApiClient new];
    //
    //    NSString *apiUrl = [api sendRequest];
    //
    //    if (apiUrl.length) {
    //        urlURL = [NSURL URLWithString:apiUrl];
    //    }
    
    self.view.backgroundColor = [UIColor colorWithRed:2./255. green:28./255. blue:32./255. alpha:1.0];
    _mainView.backgroundColor = [UIColor colorWithRed:2./255. green:28./255. blue:32./255. alpha:1.0];
    [_mainView loadRequest:req];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
